#include <Application.hpp>
#include <LightInfo.hpp>
#include <Mesh.hpp>
#include <ShaderProgram.hpp>
#include <Texture.hpp>
#include <Framebuffer.hpp>

#include <iostream>
#include <sstream>
#include <vector>

// Осторожно, этот пример скорее всего потребует OpenGL 4.5 !
// Здесь содержится ещё не разобранная тема - framebuffer.

/**
2 виртуальный камеры и 2 вьюпорта
*/
class SampleApplication : public Application
{
public:
    MeshPtr _bunny;
    MeshPtr _sphere;

    //Идентификатор шейдерной программы
    ShaderProgramPtr _commonShader;

    //Переменные для управления положением одного источника света
    float _lr = 10.0f;
    float _phi = 2.65f;
    float _theta = 0.48f;

    LightInfo _light;

    TexturePtr _brickTex;
    TexturePtr _grassTex;

    FramebufferPtr framebuffer;

    enum DepthVariants {
        ReversedFloat32,
        ReversedInt32,
        StandardInt24,
        StandardInt32,
        StandardFloat,

        TotalDepthVariants
    };

    TexturePtr depthTextures[TotalDepthVariants];
    DepthVariants currentDepthVariant;

    GLuint _sampler;

    float near = 0.1f;
    float far = 100000.0f;

    void updateCamera() {
        bool reversed = (currentDepthVariant == ReversedFloat32);

        if (reversed) {
            _camera.projMatrix[2][2] = near / (far - near);
            _camera.projMatrix[3][2] = far * near / (far - near);
        }
        else {
            _camera.projMatrix[2][2] = -(far + near) / (far - near);
            _camera.projMatrix[3][2] = -2.0f * far * near / (far - near);
        }
    }

    void applyDepthVariant(DepthVariants variant) {
        assert(variant != TotalDepthVariants);
        depthTextures[variant]->attachToFramebuffer(framebuffer->fbo(), GL_DEPTH_ATTACHMENT);

        bool reversed = (variant == ReversedFloat32 || variant == ReversedInt32);

        if (reversed) {
            // Reversed depth range, [0;1]
            glClipControl(GL_LOWER_LEFT, GL_ZERO_TO_ONE);
            _camera.projMatrix[2][2] = -near / (far - near);
            _camera.projMatrix[3][2] = -far * near / (far - near);
            glDepthFunc(GL_GREATER);
            glClearDepth(0.0f);
        }
        else {
            // Standard depth range.
            glClipControl(GL_LOWER_LEFT, GL_NEGATIVE_ONE_TO_ONE);
            _camera.projMatrix[2][2] = -(far + near) / (far - near);
            _camera.projMatrix[3][2] = -2.0f * far * near / (far - near);
            glDepthFunc(GL_LESS);
            glClearDepth(1.0f);
        }
    }

    void update() override {
        Application::update();
        updateCamera();
    }

    void makeScene() override
    {
        Application::makeScene();

        // Далеко глядим.
        _camera.projMatrix = glm::perspective(glm::radians(45.0f), 4.0f / 3.0f, near, far);

        //=========================================================
        //Создание и загрузка мешей

        _sphere = makeSphere(0.5f);

        _bunny = makeCube(0.5f);
//        _bunny = loadFromFile("models/bunny.obj");
//        _bunny->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 1.0f, 0.0f)));

        //=========================================================
        //Инициализация шейдеров
        _commonShader = std::make_shared<ShaderProgram>("shaders/common.vert", "shaders/common.frag");

        //=========================================================
        //Инициализация значений переменных освещения
        _light.position = glm::vec3(0.0f);
        _light.ambient = glm::vec3(1.0f);
        _light.specular = glm::vec3(0.0f);

        //=========================================================
        //Загрузка и создание текстур
        _brickTex = loadTexture("images/brick.jpg");
        _grassTex = loadTexture("images/grass.jpg");

        //=========================================================
        //Инициализация сэмплера, объекта, который хранит параметры чтения из текстуры
        glGenSamplers(1, &_sampler);
        glSamplerParameteri(_sampler, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glSamplerParameteri(_sampler, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glSamplerParameteri(_sampler, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glSamplerParameteri(_sampler, GL_TEXTURE_WRAP_T, GL_REPEAT);

        // Create framebuffer for fullscreen size.
        const GLFWvidmode *vidmode = glfwGetVideoMode(glfwGetWindowMonitor(_window));
        framebuffer = std::make_shared<Framebuffer>(vidmode->width, vidmode->height);

        // Create color texture and attach it to framebuffer.
        framebuffer->addBuffer(GL_RGBA, GL_COLOR_ATTACHMENT0);

        // Initialize depth textures.
        depthTextures[ReversedFloat32] = std::make_shared<Texture>();
        depthTextures[ReversedInt32] = std::make_shared<Texture>();
        depthTextures[StandardInt24] = std::make_shared<Texture>();
        depthTextures[StandardInt32] = std::make_shared<Texture>();
        depthTextures[StandardFloat] = std::make_shared<Texture>();

        depthTextures[ReversedFloat32]->setTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT32F, framebuffer->width(), framebuffer->height(), GL_DEPTH_COMPONENT, GL_FLOAT, nullptr);
        depthTextures[ReversedInt32]->setTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT32, framebuffer->width(), framebuffer->height(), GL_DEPTH_COMPONENT, GL_FLOAT, nullptr);
        depthTextures[StandardInt24]->setTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT24, framebuffer->width(), framebuffer->height(), GL_DEPTH_COMPONENT, GL_FLOAT, nullptr);
        depthTextures[StandardInt32]->setTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT32, framebuffer->width(), framebuffer->height(), GL_DEPTH_COMPONENT, GL_FLOAT, nullptr);
        depthTextures[StandardFloat]->setTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT32F, framebuffer->width(), framebuffer->height(), GL_DEPTH_COMPONENT, GL_FLOAT, nullptr);

        // Attach depth to framebuffer.
        currentDepthVariant = StandardInt24;
        applyDepthVariant(currentDepthVariant);

        glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
    }

    void updateGUI() override
    {
        Application::updateGUI();

        ImGui::SetNextWindowPos(ImVec2(0, 0), ImGuiSetCond_FirstUseEver);
        if (ImGui::Begin("MIPT OpenGL Sample", NULL, ImGuiWindowFlags_AlwaysAutoResize))
        {
            ImGui::Text("FPS %.1f", ImGui::GetIO().Framerate);

            bool dchanged = false;
            dchanged |= ImGui::RadioButton("Standard int24 depth", reinterpret_cast<int *>(&currentDepthVariant), StandardInt24);
            dchanged |= ImGui::RadioButton("Standard int32 depth", reinterpret_cast<int *>(&currentDepthVariant), StandardInt32);
            dchanged |= ImGui::RadioButton("Standard float depth", reinterpret_cast<int *>(&currentDepthVariant), StandardFloat);
            dchanged |= ImGui::RadioButton("Reverse int32 depth", reinterpret_cast<int *>(&currentDepthVariant), ReversedInt32);
            dchanged |= ImGui::RadioButton("Reverse float depth", reinterpret_cast<int *>(&currentDepthVariant), ReversedFloat32);
            if (dchanged) {
                applyDepthVariant(currentDepthVariant);
            }
        }
        ImGui::End();
    }

    void draw() override
    {
        //====== РИСУЕМ ОСНОВНЫЕ ОБЪЕКТЫ СЦЕНЫ ======
        _commonShader->use();

        //Загружаем на видеокарту значения юниформ-переменных
        _commonShader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
        _commonShader->setMat4Uniform("projectionMatrix", _camera.projMatrix);

        _light.position = glm::vec3(glm::cos(_phi) * glm::cos(_theta), glm::sin(_phi) * glm::cos(_theta), glm::sin(_theta)) * _lr;
        glm::vec3 lightPosCamSpace = glm::vec3(_camera.viewMatrix * glm::vec4(_light.position, 1.0));

        _commonShader->setVec3Uniform("light.pos", lightPosCamSpace); //копируем положение уже в системе виртуальной камеры
        _commonShader->setVec3Uniform("light.La", _light.ambient);
        _commonShader->setVec3Uniform("light.Ld", _light.diffuse);
        _commonShader->setVec3Uniform("light.Ls", _light.specular);

        glActiveTexture(GL_TEXTURE0);  //текстурный юнит 0
        glBindSampler(0, _sampler);
        _brickTex->bind();
        _commonShader->setIntUniform("diffuseTex", 0);

        framebuffer->bind();
        glViewport(0, 0, framebuffer->width(), framebuffer->height());

        int width, height;
        glfwGetFramebufferSize(_window, &width, &height);

        //Очищаем буферы цвета и глубины от результатов рендеринга предыдущего кадра
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        // Можно поругаться и сказать про инстансинг, но мы же его ещё не проходили, верно?
        auto drawSphereAndCube = [&](float x, float y, float z, float scaleXYZ) {
            glm::mat4 scale = glm::scale(glm::mat4(1.0f), glm::vec3(scaleXYZ, scaleXYZ, scaleXYZ));

            {
                _brickTex->bind();
                _sphere->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(x, y, z)) * scale);
                _commonShader->setMat4Uniform("modelMatrix", _sphere->modelMatrix());
                _commonShader->setMat3Uniform("normalToCameraMatrix", glm::transpose(
                        glm::inverse(glm::mat3(_camera.viewMatrix * _sphere->modelMatrix()))));

                _sphere->draw();
            }

            {
                _grassTex->bind();
                _bunny->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(x, y, z)) * scale);
                _commonShader->setMat4Uniform("modelMatrix", _bunny->modelMatrix());
                _commonShader->setMat3Uniform("normalToCameraMatrix", glm::transpose(
                        glm::inverse(glm::mat3(_camera.viewMatrix * _bunny->modelMatrix()))));

                _bunny->draw();
            }
        };

        // Рисуем далеко-далеко.
        for (int i = -10; i <= 10; i++) {
            for (int j = -10; j <= 10; j++) {

                float x = -90000;
                float z = i * 2000;
                float y = j * 2000;

                drawSphereAndCube(x,y,z, 1500);
            }
        }
        // Рисуем близко-близко.
        drawSphereAndCube(0, 0, 0, 0.2f);

        // Копируем на экран цвет.
        // Если уж поддерживается clip control, почему бы не зарядить dsa?
        glBlitNamedFramebuffer(framebuffer->fbo(), 0, 0, 0, width, height, 0, 0, width, height, GL_COLOR_BUFFER_BIT, GL_LINEAR);

        glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
        //Отсоединяем сэмплер и шейдерную программу
        glBindSampler(0, 0);
        glUseProgram(0);
    }
};

int main()
{
    SampleApplication app;
    app.start();

    return 0;
}